`ifndef _VERILOG_EXAMPLE_TB_
 `define _VERILOG_EXAMPLE_TB_

 `ifndef _VERILOG_EXAMPLE_
  `include "verilog_example.v"
 `endif

 `timescale 1ns/1ns

module verilog_example_tb;

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg clk;
   reg [5:0] 		inb_i;			// To UUT of verilog_example.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [6:0] 		out_o;			// From UUT of verilog_example.v
   // End of automatics


   reg [15:0] 		sin_lut[0:1023];
   reg [9:0] 		sin_lut_ptr = 0;
   wire [15:0] 		sin_out = sin_lut[sin_lut_ptr];
   
   initial begin
      $dumpfile("icarus_compile/000_verilog_example_tb.lxt");
      $dumpvars(0, verilog_example_tb);
      $readmemh("sin_lut.bin", sin_lut);

      inb_i = 0;
      clk = 1;

      #3

	#20000 inb_i = 20;
      #20000 inb_i = 10;
      #20000 inb_i = 5;

      #100 $finish;
   end

   verilog_example UUT(
		       // Outputs
		       .out_o		(out_o[6:0]),
		       // Inputs
		       .clk (clk),
		       .ina_i		(sin_out[15:10]),
		       .inb_i		(inb_i[5:0]));

   always #5 clk <= !clk;

   always begin
      #10 sin_lut_ptr <= sin_lut_ptr + 1;
   end

endmodule // verilog_example_tb

`endif
