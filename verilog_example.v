`ifndef _VERILOG_EXAMPLE_
 `define _VERILOG_EXAMPLE_

 `timescale 1ns/1ns

module verilog_example(
		       input clk,
		       input [5:0]  ina_i,
		       input [5:0]  inb_i,

		       output reg [6:0] out_o
		       );

   localparam IDLE = 0, ADD = 1;
   reg [1:0] 				state = 0;

   initial begin
      out_o = 0;
   end
   
   always @(posedge clk) begin
      case(state)
	IDLE: begin
	   if (ina_i != 6'd0) begin
	      state <= ADD;
	   end
	end
	ADD: begin
	   out_o <= ina_i + inb_i;
	   state <= IDLE;
	end
	endcase
   end
   //assign out_o = ina_i + inb_i;

endmodule // verilog_example

`endif
