#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as pl

if __name__ == "__main__":

    sin_data = np.mod(np.floor(np.sin(np.linspace(0, 2*np.pi, 1024)) * (2**15-1)),65536);

    with open("sin_lut.bin",'w') as outf:
        for k in sin_data:
            outf.write("%04x\n"%k)

    pl.plot(sin_data)
    pl.show()
